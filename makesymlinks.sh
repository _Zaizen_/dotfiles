
#!/bin/bash
############################
# makesymlinks.sh
# This script creates symlinks of the dotfiles in this repo in the right folders
############################

########## Variables

# dotfiles directory
dir=$(pwd)/dotfiles

# old dotfiles backup directory
olddir=$(pwd)/old_dotfiles

# list of files/folders to symlink in homedir
files="bashrc gitconfig functions"

##########

# create dotfiles_old in homedir
echo -n "Creating $olddir for backup of any existing dotfiles in ~ ... "
mkdir -p $olddir
echo "done"

# change to the dotfiles directory
echo -n "Changing to the $dir directory ... "
cd $dir
echo "done"


echo ${array[1]}


array=("powerlevel10k")
buffer=$(pwd)

function backup_files {
    echo $1 
    echo $2
}

function copy_files {
    if [ -e $1"/exclude.txt" ]; then
        array=$(<$1/exclude.txt)
    fi
    for i in "$1"/*;do
        if [ -d "$i" ];then
            basename_var=$(basename $i)
            if [[ ! ${array[*]} =~ ${basename_var} ]]; then
                echo "dir: $i"
                path_length=${#i}
                buffer_length=${#buffer}+1
                result_length=$path_length-$buffer_length
                result_path=${i:$buffer_length:$result_length}
                backup_files $result_path $i
                copy_files "$i"
                
            fi
        elif [ -f "$i" ]; then
            echo "file: $i"
        fi
    done
}
# So that it can list dotfiles too
shopt -s dotglob
copy_files $dir

# move any existing dotfiles in homedir to dotfiles_old directory, then create symlinks from the homedir to any files in the ~/dotfiles directory specified in $files
#echo "Moving any existing dotfiles from ~ to $olddir"
#for file in $files; do
#    mv ~/.$file ~/dotfiles_old/
#    echo "Creating symlink to $file in home directory."
#    ln -s $dir/$file ~/.$file
#done